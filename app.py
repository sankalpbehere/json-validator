
from flask import Flask, render_template, request, redirect, url_for, flash, Response
import json

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('jsonValidator.html')

@app.route('/validate' , methods = ['POST'])
def validate():
    try:
        name = str(request.form['name'])
        data = json.loads(name)
        # print("Valid JSON")
        # return redirect(url_for('login'))
        res = "You Entered Valid JSON"
        return render_template('jsonValidator.html', res=res)
    except Exception as e:
        app.logger.info(e)
        # print("Invalid JSON")
        res = "You Entered Invalid JSON"
        # return res
        return render_template ('jsonValidator.html',res = res)



if __name__ == "__main__":
    app.run( debug=True,port=5000)

